1. Introduction
---------------

REAME file for the Particle Swarm Optimization (PSO) fitter program
psofitter.py, a python script to easily fit input file parameters to
arbitrarily large training sets.

The program requires Python version 2.7 (and is written Python 3.x compatible
but some included packages are not yet migrated to Python 3) and has not been
tested for non-Linux operating systems.

This code is released under the GNU GENERAL PUBLIC LICENSE (Version 3,
29 June 2007), see the 'GPL' file for the full text.

There is a bitbucket repository available at
http://bitbucket.org/MKubillus/psofitter

2. Contact Information
----------------------

For questions about usage, updates, patches, and for bug reports please
contact Maximilian Kubillus (maximilian.kubillus@gmail.com)


3. Installation and Usage
-------------------------

Short note on requirements: psofitter uses non-standard python packages that
can usually be installed by you distribution package management or via pip
(see http://pip.pypa.io/ for more information). The current list of these
packages with versions is:

ASE >= 3.9.1 -- see http://wiki.fysik.dtu.dk/ase/

Method 1:
Execute the setup.py file

     $ python setup.py install

Method 2:
Copy the script to any location you like:

     $ mv <path-to-psofitter> ~/psofitter

Make the Python script executable (make sure you have user rights):

     $ chmod +x psofitter.py

To use the grogram you simply execute the script from your setup
folder (see below) while providing a path to the input file:

     $ psofitter.py input.cfg


4. Documentation
----------------

If running the script you need an input file which is losely based on
UNIXoid cfg-format inputs. There are two possibilities for giving inputs:

(1) One-line string inputs. Example:

    TrainingSetFile = training_set.cfg

(2) Region (multi-line) string inputs in braces. Example:

    FitParameters = {
      PSO_xi      1.0   20.0
    }

There are required and optional inputs.

Required Inputs
---------------

Name             | 1line/Region | Description
:----------------|:------------:|:----------------------------------------------
DistancesWeight  |  one-line    | float -- Weight to scale the fitness value
                 |              | based on calculated distance deviations.
                 |              |
ReactionsWeight  |  one-line    | float -- Weight to scale the fitness value
                 |              | based on calculated reaction energy
                 |              | deviations.
                 |              |
LogFile          |  one-line    | str -- Path to a logging file where all
                 |              | logging data will be written.
                 |              |
ResultFile       |  one-line    | str -- Path to a logging file where the
                 |              | results of the optimization will be written.
                 |              |
TrainingSetFile  |  one-line    | str -- Path to the training set file (see
                 |              | Training Set File section below).
                 |              |
Calculator       |              | str -- The name of the calculator to use (in
                 |              | this version only DFTB+ is supported).
                 |              |
Executable       |  one-line    | str -- Path to the executable of your quantum
                 |              | code that will be used for calculations.
                 |              |
InputTemplate    |  one-line    | str -- Calculator input file template (should
                 |              | be usable for all calculations), prepared with
                 |              | strings equivalent to those given in the Fit-
                 |              | parameters section (those will be replaced
                 |              | with PSO coordinates).
                 |              |
TrainingDir      |  one-line    | str -- Path to the directory containing all
                 |              | training systems.
                 |              |
Particles        |  one-line    | int -- Number of particles for the swarm.
                 |              |
MaxIterations    |  one-line    | int -- Maximum number of swarm iterations
                 |              | until the program is halted.
                 |              |
Tolerance        |  one-line    | float -- Convergence criterium (if the maximum
                 |              | particle coordinate fluctuation is below this
                 |              | number, the PSO is considered converged).
                 |              |
VelocityWeight   |  one-line    | float -- PSO inertia scaling parameter
                 |              |
LocalBestWeight  |  one-line    | float -- PSO local best scaling parameter
                 |              |
GlobalBestWeight |  one-line    | float -- PSO inertia scaling parameter
                 |              |
FitParameters    |   region     | str float float (name bound1 bound2) per
                 |              | line -- Input file parameters that are to
                 |              | be optimized via PSO. The given name will be
                 |              | used as (case-insensitive) replacement string
                 |              | for the input file. The boundaries for the
                 |              | parameter avoid unreasonable values from being
                 |              | generated.
                 |              |


Optional Inputs
---------------

Name             | 1line/Region | Description
:----------------|:------------:|:----------------------------------------------
Verbosity        |  one-line    | int -- Values from 0 to 2 on the amount of
                 |              | information written to files/standard output.
                 |              | Defaults to 1.
                 |              |
TemporaryBestFile|  one-line    | str -- Path to a file which will always
                 |              | contain the current global best parameters
                 |              | and fitness during a optimization.
                 |              | If the option is not given, no temporary best
                 |              | parameter file will be created.
                 |              |
NumberOfProcesses|  one-line    | int -- Maximum number of simultaneous
                 |              | processes spawned by the program.
                 |              | Defaults to 1.
                 |              |


Training Set File
-----------------

The training set file describes all observables that can be used as optimization
variables for the PSO. References have to be given line-wise in certain
observable sections (e.g. reaction energies).


Name             | 1line/Region | Description
:---------------:|:------------:|:----------------------------------------------
Molecules        |    region    | str str (name folder) per line -- Defines
                 |              | molecule names and attaches the folder name
                 |              | to do calculations with it.
                 |              |
Distances        |    region    | str Atoms=(int,int) Ref=float (molecule
                 |              | atom_numbers reference_value) per line --
                 |              | Defines a reference distance to check for a
                 |              | molecule with geometry file atom numbers and
                 |              | a reference value (given in AA).
                 |              |
Reactions        |    region    | int str + [...] -> int str + [...] Ref=float
                 |              | (# Reagent + [...] -> # Product + [...]
                 |              | Ref=reference_value) per line -- Defines a
                 |              | reaction with all its reagents and respective
                 |              | stochiometric factors (#). The reagent or
                 |              | product has to be defined in the molecules
                 |              | section. The reference value has to be given
                 |              | in kcal/mol.
                 |              |


5. Version History
------------------

v0.1: - first running development version of the program
