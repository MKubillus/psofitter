#!/bin/python3
# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.

'''psofitter.py

An implementation of particle swarm optimization to fit parameters via DFTB
calculations to an arbitrarily large training set.

Originally created to fit the Delta-Pauli extension and D3 dispersion
correction, it can be used as a program to fit any DFTB+ input parameter
to optimize observables.

For ideas and requests to extend the program write to
maximilian.kubillus@gmail.com

Maximilian Kubillus, 2015
'''

import argparse
import psofit
import sys
import textwrap

def main():
    ## Command line parser
    _apa = 'delta-pauli-fit.py - Script to fit Delta-Pauli and D3 for DFTB.'
    parser = argparse.ArgumentParser(description=_apa,
                                     epilog=('Maximilian Kubillus, Sep 2015, '
                                             'maximilian.kubillus@gmail.com'))
    _apa = 'Filename of the program input.'
    parser.add_argument('config', metavar='config.cfg', type=str,
                        action='store', nargs=1, help=textwrap.dedent(_apa))
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s 0.1')
    del _apa

    args = parser.parse_args()

    try:
        pso_fit = psofit.PSOFit(args.config[0])
        pso_fit.run_optimization()
    except KeyboardInterrupt:
        print('\n\nCaught termination signal. Aborting PSO procedure!\n')
        print(pso_fit.get_global_best())
        sys.exit(-1)
    except:
        print(pso_fit.get_global_best())
        raise

if __name__ == '__main__':
    main()
else:
    pass

