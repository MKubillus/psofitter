#!/usr/bin/env python3

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.

from distutils.core import setup

setup(name='PSOFitter',
      version='0.1',
      description='Particle Swarm Optimization fitting for quantum chemistry',
      author='Maximilian Kubillus',
      author_email='maximilian.kubillus@gmail.com',
      url='https://bitbucket.org/MKubillus/psofitter',
      packages=['psofit'],
      license='GNU GPL 3'
     )
