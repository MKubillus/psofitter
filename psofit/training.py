'''
training -- Handling the training set and references
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

import os
import sys
from . import parser

## Global variables you might want to modify
REAC_ARROW = '->' # Reaction arrow definition string

## Global constants you should /not/ modify
AU_2_KCAL = 627.509 # Conversion factor for E_h to kcal/mol
KCAL_2_AU = 1.0 / AU_2_KCAL # Conversion factor for kcal/mol to E_h

class TrainingSet(object):
    '''Contains all training systems, references and calculates results.'''

    def __init__(self, train_file, training_dir):
        '''Initializes the instance.'''
        self.MOL_SYNTAX_STR = 'Syntax: Molecule_Name   path/to/directory'
        self.REAC_SYNTAX_STR = 'Syntax: # Reagent1 + # Reagent2 ' + \
                     ' %s # Product1 + # Product2 Ref=123.4' % (REAC_ARROW) + \
                     '\n(with # being a stochiometric number and the ' + \
                     'energy given in kcal/mol'
        self.DIST_SYNTAX_STR = 'Syntax: Molecule_Name  Atoms=(1,2)  Ref=' + \
                               '1.542 (with the reference given in Angstrom)'
        self.training_dir = training_dir
        self.training_set = self._read_training_set(train_file)
        if (self.distances and self.reactions):
            print('Training set consisting of distances and reaction ' + \
                  'energies read.')
        elif (self.distances and not self.reactions):
            print('Training set consisting of distances read.')
        elif (not self.distances and self.reactions):
            print('Training set consisting of reaction energies read.')
        else:
            sys.exit('Error: Input contained no training observables')


    def get_molecule(self, mol_name):
        '''Returns molecule object matching given name.'''
        for mol in self.molecules:
            if (mol.get_lname() == mol_name.lower()):
                return mol
        return None


    def get_reactions(self):
        '''Returns a list of all reference reactions.'''
        return self.reactions


    def get_distances(self):
        '''Returns a list of all reference distances.'''
        return self.distances


    ## Private methods, all may be subject to major changes
    def _read_training_set(self, train_file):
        '''Reads the training set file and sets up the systems.'''
        ## Read the file and remove comments and whitespace
        with open(train_file, 'r') as ff:
            in_file = ff.readlines()
            in_file = [line[:line.find(parser.COMMENT)] for line in in_file]
        ## Pass different sections of the training set file
        if (not self._parse_molecules(in_file)):
            sys.exit('Error in parsing molecule definitions.')
        self._parse_reactions(in_file)
        self._parse_distances(in_file)


    def _parse_molecules(self, in_file):
        '''Reads the molecule definitions section of the training set file and
        returns a list with all molecule dictionaries.'''
        data = parser.read_region(in_file, 'Molecules')
        if (not data):
            sys.exit('Error: Required input region "Molecules" not found in '
                     'training set file.')
        self.molecules = []
        for line in data:
            mol = line.split()
            ## Check amount of entries in line and add the molecule if correct
            if (len(mol) > 2):
                sys.exit('Too many arguments for molecule ' + \
                         'definition:\n"%s"\n' % line + self.MOL_SYNTAX_STR)
            elif (len(mol) < 2):
                sys.exit('Not enough arguments for molecule ' + \
                         'definition:\n"%s"\n' % line + self.MOL_SYNTAX_STR)
            else:
                ## In this list molecule names are not saved lowercase since we
                ## want the correct input/output spelling, all observables will
                ## be lowercase, though.
                self.molecules.append(Molecule(mol[0].strip(), '%s/%s' % \
                                               (self.training_dir,
                                                mol[1].strip())))
        return True


    def _parse_reactions(self, in_file):
        '''Reads the reaction definitions section of the training set file and
        returns a list with all reaction dictionaries.'''
        data = parser.read_region(in_file, 'Reactions')
        self.reactions = []
        for line in data:
            i_arrow = line.find(REAC_ARROW) # Split index for reagents/products
            if (not i_arrow): # Invalid reaction failsafe
                sys.exit('Invalid reaction definition: Reaction arrow ' + \
                         'missing in definition:\n"%s"\n' % line +
                         self.REAC_SYNTAX_STR)
            self._mols = []
            self._factors = []
            self._ref = 0.0
            data = line[:i_arrow]
            self._get_reaction_side(data, 'reagents')
            data = line[i_arrow+len(REAC_ARROW):]
            self._get_reaction_side(data, 'products')
            self.reactions.append(Reaction(self._mols, self._factors,
                                           self._ref))
        return True


    def _get_reaction_side(self, data, side):
        '''Takes the given side of a reaction definition and parses it into
        temporary instance variables.'''
        entries = data.strip().split()
        if (side == 'reagents'):
            prefac = -1 # Reagents get a minus in the energy calculation
        else:
            prefac = 1 # Products
        ii = 0
        while (ii < len(entries)):
            ## If the entry is a plus sign, raise indexer
            if (entries[ii].strip() == '+'):
                ii += 1
            ## Put the stochiometric factor in the factors list. If it is not a
            ## valid integer check whether it is the reference energy.
            try:
                self._factors.append(prefac * int(entries[ii]))
            except ValueError:
                ## Check whether it's the reference argument
                if (side == 'products'):
                    self._ref = self._get_reference(entries[ii])
                    break
                else:
                    sys.exit('"%s" is not a valid stochiometric factor ' + \
                             'for a reaction in definition:\n"%s"\n' % \
                             (entries[ii], data) + self.REAC_SYNTAX_STR)
            ii += 1
            ## Add given molecule to list if is in the molecule definitions.
            if (self._in_molecules(entries[ii])):
                self._mols.append(entries[ii].lower())
            else:
                sys.exit('Molecule "%s" not in Molecules ' % entries[ii] + \
                         'section. Error in reaction definition:\n' + \
                         '"%s"' % data)
            ii += 1
        return True


    def _get_reference(self, reference_str):
        '''Uses a reference string and returns the given value.'''
        if (reference_str.lower().find('ref') >= 0): # If not found: -1
            try:
                reference_str = reference_str.split('=')[-1].strip()
                ref = float(reference_str)
            except ValueError:
                sys.exit('Error: "%s" is not a valid ' % reference_str + \
                         'energy value for reaction.' + self.REAC_SYNTAX_STR)
        else:
            sys.exit('Error: Unknown or misplaced argument "%s".' % \
                     reference_str)
        return ref


    def _parse_distances(self, in_file):
        '''Reads the distance definitions section of the training set file and
        returns a list with all distance dictionaries.'''
        data = parser.read_region(in_file, 'Distances')
        self.distances = []
        for line in data:
            entries = line.split()
            ## Check if molecule is defined
            if (self._in_molecules(entries[0])):
                mol = entries[0].lower().strip()
            else:
                sys.exit('Molecule "%s" not in Molecules ' % entries[0] + \
                         'section. Error in distance definition:\n' + \
                         '"%s"' % line)
            atoms = self._get_dist_atoms(entries[1])
            ref = self._get_reference(entries[2])
            self.distances.append(Distance(mol, atoms, ref=ref))
        return True


    def _get_dist_atoms(self, atom_str):
        '''Takes an atoms definition string and returns the given atom numbers
        as a list.'''
        tmp_str = atom_str.strip().lower()
        ## Check if the given string is indeed a atom definition
        if (tmp_str[0:5] != 'atoms'):
            sys.exit('Error: Unknown argument for distance definition: %s' % \
                     (atom_str))
        ## Grab the numbers between the parentheses
        l_parenth = tmp_str.find('(') + 1
        r_parenth = tmp_str.find(')')
        tmp_str = tmp_str[l_parenth:r_parenth].split(',')
        ## Check the amount of given atoms
        if (len(tmp_str) > 2):
            sys.exit('Error: Too many atoms given for a distance ' + \
                     'calculation: %s' % atom_str)
        elif (len(tmp_str) < 2):
            sys.exit('Error: Not enough atoms given for a distance ' + \
                     'calculation: %s' % atom_str)
        ## Now build the atom list
        atoms = []
        for atom in tmp_str:
            try:
                atoms.append(int(atom))
            except ValueError:
                sys.exit('Error: %s is not a valid atom number in %s.' % \
                         (atom, atom_str))
        return atoms


    def _in_molecules(self, name):
        '''Checks whether the given name is in the molecules list.'''
        for molecule in self.molecules:
            if (name.lower() == molecule.get_lname()):
                return True
        return False


class Molecule:
    '''Defines a molecule.'''

    def __init__(self, name, folder, energy=None, distance=None):
        '''Initializes the molecule.'''
        if (type(name) == str):
            self.name = name
        else:
            sys.exit('Error: Caught invalid molecule name.')
        if (os.path.exists(folder)):
            self.folder = folder
        else:
            sys.exit('Error: Path for molecule %s does not exist.' % self.name)
        if (type(energy) == float or energy == None):
            self.energy = energy
        else:
            sys.exit('Error: Given energy for molecule %s not a floating ' \
                     'point number.' % self.name)
        if (type(distance) == Distance or distance == None):
            self.distance = distance
        else:
            sys.exit('Error: Given distance for molecule %s not a distance ' \
                     'object.')


    def get_name(self):
        '''Returns the molecule name as saved.'''
        return self.name


    def get_lname(self):
        '''Returns the lower-case molecule name.'''
        return self.name.lower()


    def get_folder(self):
        '''Returns the molecule folder name.'''
        return self.folder


    def get_energy(self):
        '''Returns the molecule total energy.'''
        if (self.energy):
            return self.energy
        else:
            return False


    def get_distance(self):
        '''Returns the saved distance.'''
        if (self.distance):
            return self.distance.get_distance()
        else:
            return False


    def update_energy(self, energy):
        '''Updates the object with a new energy value.'''
        if (type(energy) == float):
            self.energy = energy
        else:
            sys.exit('Error: Argument %s is not of type float.' % energy)


    def update_distance(self, dist):
        '''Updates the object with a new distance value.'''
        if (type(dist) == Distance):
            self.distance = dist
        elif (type(dist) == float):
            self.distance.update(dist)
        else:
            sys.exit('Error: Argument %s is not of type Distance or float.' % \
                     dist)


class Reaction:
    '''Defines a single reaction.'''

    def __init__(self, mol_names, factors, ref=False, val=False):
        '''Initializes the reaction.'''
        self.molecules = mol_names
        for factor in factors:
            if (type(factor) != int):
                sys.exit('Error: %s is not a valid stochiometric factor' % \
                         factor)
        self.factors = factors
        if (type(ref) == float and val == False):
            self._is_reference = True
            self.ref = ref * KCAL_2_AU
        elif (type(val) == float and ref == False):
            self._is_reference = False
            self.value = val * KCAL_2_AU
        elif (val == False and ref == False):
            self._is_reference = False
        else:
            sys.exit('Error: Reaction object cannot be reference and ' + \
                     'test system at the same time.')


    def update(self, molecules):
        '''Updates the reaction energy with energy values from the given
        molecule list.'''
        if (not self._check_list_validity(molecules)):
            sys.exit('Error: Given molecule list is not compatible with' + \
                     'reaction.')
        self.val = 0.0
        for mol in molecules:
            ii = 0
            while (ii < len(self.molecules)):
                if (mol.get_lname() == self.molecules[ii]):
                    self.val += mol.get_energy() * \
                                float(self.factors[ii])
                    break
                else:
                    ii += 1
        return None


    def get_energy(self):
        '''Returns the energy value.'''
        if (self._is_reference):
            return self.get_ref()
        else:
            return self.get_val()


    def get_ref(self):
        '''Returns the reference value if reaction system is a reference.'''
        if (not self._is_reference):
            sys.exit('Error: Reaction is not a reference.')
        return self.ref


    def get_val(self):
        '''Returns the reference value if reaction system is a reference.'''
        if (self._is_reference):
            sys.exit('Error: Reaction is not a test system.')
        return self.val


    def get_molecules(self):
        '''Returns the list of molecules in correct index order.'''
        return self.molecules


    def get_factors(self):
        '''Returns the stochiometric factor of a molecule in the
        reaction.'''
        return self.factors


    def get_deviation(self, energy):
        '''Returns the deviation of the given energy from the reference.'''
        if (not self._is_reference):
            sys.exit('Error: Reaction is not a reference.')
        try:
            deviation = (energy - self.ref) * AU_2_KCAL
        except ValueError:
            sys.exit('Error: Invalid energy value %s!' % energy)
        return deviation


    ## Private methods, all may be subject to major changes
    def _check_list_validity(self, molecules):
        '''Checks a given molecule list for compatibility with the reaction.'''
        input_mol_names = []
        for mol in molecules:
            input_mol_names.append(mol.get_lname())
        input_mol_names.sort()
        reac_mol_names = list(self.molecules)
        reac_mol_names.sort()
        intersect = set(input_mol_names)
        intersect = intersect.intersection(reac_mol_names)
        if (len(intersect) == len(self.molecules)):
            return True
        else:
            return False


class Distance:
    '''Defines a distance.'''

    def __init__(self, mol, atoms, ref=False, val=False):
        '''Initializes the distance.'''
        self.molecule = mol
        for atom in atoms:
            if (type(atom) != int):
                sys.exit('Error: %s is not a valid atom number' % atom)
        self.atoms = atoms
        if (type(ref) == float and val == False):
            self._is_reference = True
            self.ref = ref
        elif (type(val) == float and ref == False):
            self._is_reference = False
            self.value = val
        elif (val == False and ref == False):
            self._is_reference = False
        else:
            sys.exit('Error: Distance object cannot be reference and ' + \
                     'test system at the same time.')


    def update(self, dist):
        '''Updates the given distance value.'''
        if (not self._is_reference):
            self.distance = dist
        else:
            sys.exit('Error: Trying to update reference distance object.')


    def get_atoms(self):
        '''Returns the atoms list.'''
        return self.atoms


    def get_distance(self):
        '''Returns the reference value.'''
        if (self._is_reference):
            return self.get_ref()
        else:
            return self.get_val()


    def get_ref(self):
        '''Returns the reference value if reaction system is a reference.'''
        if (not self._is_reference):
            sys.exit('Error: Distance is not a reference.')
        return self.reference


    def get_val(self):
        '''Returns the reference value if reaction system is a reference.'''
        if (self._is_reference):
            sys.exit('Error: Distance is not a test system.')
        return self.value


    def get_name(self):
        '''Returns the lower-case molecule name.'''
        return self.molecule.lower()


    def get_deviation(self, dist):
        '''Returns the deviation of the given distance from the
        reference.'''
        if (not self._is_reference):
            sys.exit('Error: Reaction is not a reference system.')
        try:
            deviation = (dist - self.ref)
        except ValueError:
            sys.exit('Error: Invalid distance value %s!' % distance)
        return deviation
