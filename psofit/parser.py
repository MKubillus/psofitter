'''
parser -- Handles reading inputs.
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

import re
import sys

## WILL LATER CONTAIN ALL PARSER ELEMENTS BUT FOR NOW IT'S ONLY THE SHARED
## FUNCTION READ_REGION

COMMENT = '#' # Comment symbol
REG_OPEN = '{' # Definition string for opening a region input
REG_CLOSE = '}' # Definition string for closing a region input

def read_region(in_file, search_string):
    '''Looks for the given searching string in the file and returns
    the whole multi-line input region line by line, stripped of the
    enclosing symbols and any whitespace.'''
    region = []
    regexpr = re.compile(r'^%s\s*=\s*.*$' % search_string, re.I)
    file_length = len(in_file)
    ii = 0
    ## Find starting line.
    while ii < file_length:
        line = in_file[ii]
        match = regexpr.match(line)
        if (match):
            ## If a matching region opening is found, add the line to region
            region.append(line[line.find(REG_OPEN)+1:].strip())
            break
        else:
            pass
        ii += 1
    if (not match): # No matches found in the whole file.
        return False
    else:
        pass
    ## Complete the input region.
    ii += 1
    while ii < file_length:
        line = in_file[ii]
        ii += 1
        ## Check whether region is closed in this line. If not, add the line
        ## to the region as long as it isn't the last line of the file.
        try:
            region.append(line[:line.index(REG_CLOSE)].strip())
            break
        except ValueError:
            if (ii == file_length):
                sys.exit('Input Error: There are open input regions' + \
                         'at the end of the training set file!' + \
                         '\nError found on line in section:\n%s' % \
                         match.group(0))
            else:
                region.append(line.strip())
    region = [x for x in region if x.strip()] # Remove empty strings
    return region
