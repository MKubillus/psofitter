'''
swarm -- Particle Swarm optimization and particle handling.
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

import sys
from random import uniform, random

## Constants
FIT_TOL = 1e-5 # Tolerance for global best fitness

class ParticleSwarm(object):
    '''Particle Swarm Optimization (PSO). This iterable class controls all
    particles and the optimization process.'''

    def __init__(self, n_part, m_iter, tolerance, velocity_weight,
                 local_best_weight, global_best_weight, pso_coord):
        '''Initializes the particle swarm with all optimization parameters.'''
        self.COORD_SYNTAX_STR = 'Syntax: parameter_name  bound1  bound2'
        ## Input parameters
        self.n_part = n_part # Swarm size (total no. of particles)
        self.m_iter = m_iter # Maximum iterations
        self.tolerance = tolerance # Stop criterion when fitness is 'converged'
        self.w_local_best = local_best_weight
        self.w_global_best = global_best_weight
        self.w_velocity = velocity_weight
        self.pso_params = [self.w_velocity, self.w_local_best,
                           self.w_global_best]
        if (not self._parse_coords(pso_coord)):
            sys.exit('Error: Couldn\'t read PSO fit parameters.')
        self._iter_init()


    def step(self, new_fitness):
        '''Updates all particles based on the new fitness values
        and returns new coordinates for all particles.'''
        if ((self.i_iter % 5 == 0) and (self.i_iter != 0)):
            print('Iteration %s' % self.i_iter)
        self.new_best = False
        particle_xx = []
        i_part = 0
        while (i_part < self.n_part):
            tmp_xx = self.next_particle(new_fitness[i_part], i_part)
            particle_xx.append(tmp_xx)
            i_part += 1
        self._determine_global_best()
        self.i_iter += 1
        return particle_xx


    def next_particle(self, fit_val, i_part):
        '''Updates the particle index with the given fitness value and
        returns its new coordinates.'''
        if (self.particles[i_part].update(self.global_best_xx, fit_val,
                                          self.pso_params, self.coord)):
            self.m_delta = max(self.m_delta, self.particles[i_part].get_delta())
            particle_xx = self.particles[i_part].get_coord()
        else:
            sys.exit('Error: Particle update failed.')
        return particle_xx


    def is_finished(self):
        '''Returns wether the PSO is done.'''
        if (self.i_iter > self.m_iter or self.m_delta < self.tolerance):
            return True
        else:
            return False


    def get_particle_coords(self):
        '''Returns all current particle coordinates.'''
        all_xx = []
        for pp in self.particles:
            all_xx.append(pp.xx)
        return all_xx


    def new_global_best(self):
        '''Returns whether there is a new global best fitness.'''
        return self.new_best


    def get_global_best(self):
        '''Returns the current global best parameters and the fitness obtained
        with them.'''
        return (self.global_best_xx, self.global_best_fitness)


    ## Private methods, all may be subject to major changes
    def _iter_init(self):
        '''Prepares iteration of the particle swarm and returns the updated
        instance for use with next().'''
        self.i_iter = 0 # Current iteration number
        self.m_delta = sys.float_info.max # Maximum coordinate delta
        self.global_best_fitness = sys.float_info.max
        self.global_best_xx = [None] * len(self.coord)
        self.converged = False
        self.new_best = False # Whether new best global fitness was found
        self.particles = []
        ii = 0
        while (ii < self.n_part):
            self.particles.append(Particle(self.coord))
            ii += 1
        return


    def _determine_global_best(self):
        '''Looks for a new global best after an iteration step.'''
        for pp in self.particles:
            pp_fitness = pp.get_fitness()
            if (self.global_best_fitness - FIT_TOL > pp_fitness):
                self.new_best = True
                self.global_best_fitness = pp_fitness
                self.global_best_xx = pp.get_last_coord()
        return None


    def _parse_coords(self, pso_coord):
        '''Parses the list of PSO coordinates and returns a dictionary for each
        coordinate.'''
        self.coord = []
        for coord in pso_coord:
            entries = coord.split()
            ## Check line inputs for correctness.
            if (len(entries) < 3):
                sys.exit('Error: Not enough arguments for PSO parameter:' + \
                         '\n%s\n' % coord + self.COORD_SYNTAX_STR)
            elif (len(entries) > 3):
                sys.exit('Error: Too many arguments for PSO parameter:' + \
                         '\n%s\n' % coord + self.COORD_SYNTAX_STR)
            else:
                try:
                    bounds = [float(entries[1]), float(entries[2])]
                except ValueError:
                    sys.exit('Error: Invalid bound for PSO parameter '
                             '%s: %s\n' % (entries[0], ' '.join(entries[1:])) +
                             self.COORD_SYNTAX_STR)
            if (bounds[0] < bounds[1]):
                self.coord.append({'name': entries[0], 'lb': bounds[0],
                                   'ub': bounds[1]})
            else:
                self.coord.append({'name': entries[0], 'lb': bounds[1],
                                   'ub': bounds[0]})
        return True


class Particle:
    '''An iterable PSO class containing information for one particle.'''

    def __init__(self, coord):
        '''Initializes a particle.'''
        self.n_dims = len(coord)
        self.xx = [None] * self.n_dims # Particle coordinates
        self.vv = [None] * self.n_dims # Particle velocity
        self.xx_last = [None] * self.n_dims
        ii = 0
        while (ii < self.n_dims):
            self.xx[ii] = uniform(coord[ii]['lb'], coord[ii]['ub'])
            self.vv[ii] = uniform(-abs(coord[ii]['ub'] - coord[ii]['lb']),
                                  abs(coord[ii]['ub'] - coord[ii]['lb']))
            ii += 1
        self.local_best_fitness = sys.float_info.max # Starts with "infinity"
        self.local_best_xx = self.xx
        self.fitness = sys.float_info.max # Starts with "infinity"
        self.delta = 0.0 # Difference between last and new coordinates
        self.updated = False


    def update(self, global_best_xx, new_fitness, pso_params, coord):
        '''Updates the particle for the next step.'''
        if (new_fitness):
            self.fitness = new_fitness
        if (self.fitness < self.local_best_fitness):
            self.local_best_fitness = float(self.fitness)
            self.local_best_xx = list(self.xx)
        ## Update velocity with the PSO parameters
        ii = 0
        while (ii < self.n_dims):
            self.vv[ii] = self._update_velocity(self.vv[ii],
                                                pso_params, global_best_xx[ii],
                                                self.local_best_xx[ii],
                                                self.xx[ii])
            ## Save the current coordinates as 'last' before updating them
            self.xx_last[ii] = float(self.xx[ii])
            self.xx[ii] += self.vv[ii] # Update coord with new velocity
            self.delta = max(self.delta, self.xx[ii] - self.xx_last[ii])
            ## Check for bounds ande replace coordinates accordingly
            if (self.xx[ii] < coord[ii]['lb']):
                self.xx[ii] = coord[ii]['lb']
            elif(self.xx[ii] > coord[ii]['ub']):
                self.xx[ii] = coord[ii]['ub']
            ii += 1
        return True


    def get_delta(self):
        '''Returns a copy of the current particle maximum delta.'''
        return float(self.delta)


    def get_coord(self):
        '''Returns a copy of the current particle coordinates.'''
        return list(self.xx)


    def get_last_coord(self):
        '''Returns a copy of the current particle coordinates.'''
        return list(self.xx_last)


    def get_best_coord(self):
        '''Returns a copy of the current local best particle coordinates.'''
        return list(self.local_best_xx)


    def get_fitness(self):
        '''Returns a copy of the current particle fitness.'''
        return float(self.fitness)


    def get_best_fitness(self):
        '''Returns a copy of the current local best particle fitness.'''
        return float(self.local_best_fitness)


    ## Private methods, all may be subject to major changes
    @staticmethod
    def _update_velocity(old_vv, pso_params, global_best, local_best, current):
        '''Returns the new velocity for one coordinate.'''
        ## Velocity term from "inertia" (last step)
        vv = pso_params[0] * old_vv
        ## Velocity term from local best
        vv += pso_params[1] * random() * (local_best - current)
        ## Velocity term from global best
        if (global_best):
            delta = (global_best - current)
        else:
            delta = 0.0
        vv += pso_params[2] * random() * delta
        return vv
