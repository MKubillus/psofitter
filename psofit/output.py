'''
output -- Handling writing operations.
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

import sys
from . import swarm

BAR = '=' * 30 + '\n' # Shortcut string variable for separation bars in output

class Output(object):
    '''Writes all output data to files or standard output.'''

    def __init__(self, log_file, results_file, verbosity):
        '''Initializes the instance.'''
        self.log = open(log_file, 'w')
        self.results = open(results_file, 'w')
        self.vv = verbosity


    def write_init(self, swarm):
        '''Writes text describing the initial setup, information depending
        on the verbosity level.'''
        init_status = ['Particle Swarm Optimization (PSO) setup:']
        init_status.append('Swarm size: %s' % swarm.n_part)
        init_status.append('PSO weights:')
        init_status.append('  Inertia: %s\n  Local Best: %s\n' % \
                           (swarm.pso_params[0], swarm.pso_params[1]) + \
                           '  Global Best: %s\n' % swarm.pso_params[2])
        init_status.append('Particle Coordinates:\n')
        for coord in swarm.coord:
            init_status.append('%s ranging from %s to %s.' % \
                           (coord['name'], coord['lb'], coord['ub']))
        init_status.append('\n%s' % BAR)
        ## Write this basic information for verbosity 0 and 1.
        if (self._write_to_all(init_status)):
            init_status = []
        else:
            sys.exit('Unknown error while writing output.')
        if (self.vv > 1):
            ## Write informration about all particles for verbosity 2.
            init_status.append('Initial particle values')
            init_status.append(self._get_all_particles(swarm))
            init_status.append(' ')
            if (self._write_to_all(init_status)):
                return True
            else:
                sys.exit('Unknown error while writing output.')
        else:
            return True


    def write_status(self, swarm):
        '''Writes update text of the current iteration, depending on
        the verbosity level.'''
        if (self.vv == 0):
            return [] # Never write anything
        elif (self.vv >= 1 and swarm.new_global_best() == True):
            ## Write the new global best if it has changed
            iter_status = ['\n']
            if (swarm.i_iter == 1):
                iter_status.append('Initial global best fitness: %s' % \
                                   swarm.global_best_fitness)
            else:
                iter_status.append('New global best fitness %s ' % \
                                   swarm.global_best_fitness + \
                                   'at iteration %s!' % swarm.i_iter)
            iter_status.append('with coordinates:')
            iter_status.append(self.get_global_best_coordinates(swarm))
            ## Write stdout output
            if (self.vv == 1):
                self._write_to_stdout([iter_status[1]])
            elif (self.vv == 2):
                self._write_to_stdout(iter_status)
                iter_status.append(BAR + '\nParticle coordinates:')
                iter_status.append(self._get_all_particles(swarm))
            self._write_to_log(iter_status)
        if (self.vv == 2):
            ## Write out all particles and their coordinates
            iter_status = ['%s' % BAR, 'Particle coordinates:']
            iter_status.append(self._get_all_particles(swarm))
            iter_status.append('\n')
            self._write_to_log(iter_status)


    def write_result(self, swarm):
        '''Writes text into the results file for the termination of the program,
        returning iteration number and the global best fitness and the
        coordinates yielding it.'''
        results = ['\n%s' % BAR]
        results.append('\nPSO finished! Final Results:\n')
        results.append('Iterations: %s\nGlobal best fitness: %s\n' % \
                       (swarm.i_iter - 1, swarm.global_best_fitness))
        results.append('Obtained with coordinates:\n')
        results.append('%s\n' % self.get_global_best_coordinates(swarm))
        if (self._write_to_results(results)):
            return True
        else:
            ## Failsafe to not lose all data when results cannot be written.
            print('Unknown error while writing out results.\n' +
                  'Trying to dump results text into standard output...')
            self._write_to_stdout(results)
            sys.exit()


    @staticmethod
    def get_global_best_coordinates(swarm):
        '''Returns a string containing the coordinate names and values yielding
        the current global best fitness.'''
        global_best = []
        ii = 0
        while ii < len(swarm.coord):
            global_best.append('%s: %s' % (swarm.coord[ii]['name'],
                                           swarm.global_best_xx[ii]))
            ii += 1
        return '\n'.join(global_best)


    ## Private methods, all may be subject to major changes
    def _write_to_log(self, lines):
        '''Writes a list of lines to the log file.'''
        try:
            self.log.write('\n'.join(lines))
            return True
        except:
            sys.exit('Error writing to log file!')


    def _write_to_results(self, lines):
        '''Writes a list of lines to the results file.'''
        try:
            self.results.write('\n'.join(lines))
            return True
        except:
            sys.exit('Error writing to results file!')


    def _write_to_stdout(self, lines):
        '''Writes a list of lines to the standard output.'''
        print('\n'.join(lines))
        return True


    def _write_to_all(self, lines):
        '''Writes a list of lines to log file, the results file, and to the
        standard output.'''
        out_text = '\n'.join(lines)
        try:
            self.log.write(out_text)
        except:
            sys.exit('Error writing to log file!')
        try:
            self.results.write(out_text)
        except:
            sys.exit('Error writing to results file!')
        print(out_text)
        return True


    @staticmethod
    def _get_all_particles(swarm):
        '''Returns a string containing complete information of all particles.'''
        part_status = []
        i_part = 0
        while (i_part < swarm.n_part):
            part_status.append('\nParticle %s' % (i_part + 1))
            ii = 0
            while (ii < len(swarm.coord)):
                part_status.append('  %s: %s' % (swarm.coord[ii]['name'],
                                                 swarm.particles[i_part].xx[ii]))
                ii += 1
            i_part += 1
        return '\n'.join(part_status)
