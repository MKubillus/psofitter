'''
psofit -- Main package handler.
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

import re
import sys
from . import calculator
from . import output
from . import parser
from . import swarm
from . import training
from functools import partial
from multiprocessing import Pool


class PSOFit(object):
    '''Handles the PSOFit procedures.'''

    def __init__(self, config_file):
        self.config_file = self._read_input_file(config_file)
        self.output = self._init_output()
        self.training = self._init_train()
        self.calculator = self._init_calculator()
        self.swarm = self._init_swarm()
        self.n_proc = int(self._get_input('NumberOfProcesses', required=False,
                                          default='1'))
        self.w_distances = float(self._get_input('DistancesWeight'))
        self.w_reactions = float(self._get_input('ReactionsWeight'))
        self.temp_best_file = self._get_input('TemporaryBestFile',
                                              required=False, default=None)


    def run_optimization(self):
        '''Handler that runs standard PSO optimization.'''
        self.all_xx = self.swarm.get_particle_coords()
        self.output.write_init(self.swarm)
        while (not self.swarm.is_finished()):
            self._run_calculations()
            self.all_xx = self.swarm.step(self._fitness)
            self.output.write_status(self.swarm)
            if (self.temp_best_file):
                with open('./%s' % self.temp_best_file, 'w') as tmp_log:
                    tmp_log.write('%s\n' % self.get_global_best())
        self.output.write_result(self.swarm)
        return None


    def get_global_best(self):
        '''Returns the global best fitness and parameters as a human-readable
        printable string.'''
        (best_xx, best_fitness) = self.swarm.get_global_best()
        best_params = ['Global best fitness: %s' % best_fitness,
                       'Obtained with these parameters:']
        ii = 0
        while (ii < len(best_xx)):
            best_params.append('  %s: %s' % (self.swarm.coord[ii]['name'],
                                             best_xx[ii]))
            ii += 1
        return '\n'.join(best_params)


    ## Private methods, all may be subject to major changes
    @staticmethod
    def _read_input_file(file_path):
        '''Reads a input file, removes comments and returns each
        line as a string, stripped from all empty elements.'''
        with open(file_path, 'r') as ff:
            in_file = ff.readlines()
            in_file = [line[:line.find(parser.COMMENT)] for line in in_file]
            in_file = [x for x in in_file if x]
        return in_file


    def _init_output(self):
        '''Sets up needed variables for the logging object and instantiates.'''
        ## Parse required input variables
        self.log = self._get_input('LogFile')
        self.results = self._get_input('ResultFile')
        ## Parse optional input variables
        self.verbosity = int(self._get_input('Verbosity', required=False,
                                             default='0'))
        return output.Output(self.log, self.results, self.verbosity)


    def _init_train(self):
        '''Parses the config file and sets a reference training set.'''
        self.training_dir = self._get_input('TrainingDir')
        self.train_file = self._get_input('TrainingSetFile')
        return training.TrainingSet(self.train_file, self.training_dir)


    def _init_calculator(self):
        '''Parses the input file and sets up the calculator.'''
        self.calc_name = self._get_input('Calculator')
        self.executable = self._get_input('Executable')
        self.input_template = self._get_input('InputTemplate')
        return calculator.Calculator(self.calc_name, self.executable,
                                     self.input_template)


    def _init_swarm(self):
        '''Parses the config file and sets up needed variables.'''
        ## Parse required input variables
        self.n_part = int(self._get_input('Particles'))
        self.m_iter = int(self._get_input('MaxIterations'))
        self.tolerance = float(self._get_input('Tolerance'))
        self.velocity_weight = float(self._get_input('VelocityWeight'))
        self.local_best_weight = float(self._get_input('LocalBestWeight'))
        self.global_best_weight = float(self._get_input('GlobalBestWeight'))
        self.pso_coord = parser.read_region(self.config_file, 'FitParameters')
        return swarm.ParticleSwarm(self.n_part, self.m_iter, self.tolerance,
                                   self.velocity_weight, self.local_best_weight,
                                   self.global_best_weight, self.pso_coord)


    def _get_input(self, option, required=True, default=False):
        '''Checks wether a required input option is given and returns it.
        In case of missing input the function checks if it is a required option.
        If this is the case the program will be halted, if not the default value
        will be returned.'''
        regexpr = re.compile(r'^%s\s*=\s*(.*?)$' % option, re.I)
        try:
            tmpInp = self._find_regexpr(self.config_file, regexpr).group(1)
        except AttributeError:
            if (required == True):
                sys.exit('Required input option %s not found. Aborting...' % \
                         option)
            else:
                tmpInp = default
        return tmpInp


    @staticmethod
    def _find_regexpr(search_file, regexpr):
        '''Looks for the given compiled regular expression in a list of strings
        and returns the match of the expression or None if no match is found.'''
        for line in search_file:
            match = regexpr.match(line)
            if (match):
                break
        if (match):
            return match
        else:
            return None


    def _run_calculations(self):
        '''Runs the calculator on all given molecules and
        updates fitness values.'''
        self._fitness = []
        i_part = 0
        while i_part < self.n_part:
            folders = []
            for mol in self.training.molecules:
                folders.append(mol.get_folder())
            ## Run calculator on all training set molecules with parameters,
            ## using multiprocessing
            try:
                pool = Pool(processes=self.n_proc)
                prun_molecule = partial(self.calculator.run,
                                        coord=self.swarm.coord,
                                        values=self.all_xx[i_part])
                pool.map(prun_molecule, folders)
            except KeyboardInterrupt:
                pool.terminate()
                pool.close()
                pool.join()
                raise KeyboardInterrupt
            ## Close the pool
            pool.close()
            pool.join()
            self._eval_fitness()
            i_part += 1


    def _eval_fitness(self):
        '''Evaluates one fitness value based on the given training set.'''
        ## Update all energies from calculation results
        for mol in self.training.molecules:
            mol.update_energy(self.calculator.get_energy(mol.get_folder()))
        ## Evaluate distance deviations
        deviations = []
        i_dist = 0
        while i_dist < len(self.training.distances):
            deviations.append(self._calc_dist_deviation(i_dist))
            i_dist += 1
        if (len(deviations) > 0):
            self.dist_deviation = Deviation(deviations)
        else:
            self.dist_deviation = False
        deviations = []
        i_reac = 0
        ## Evaluate reaction energy deviations
        while i_reac < len(self.training.reactions):
            deviations.append(self._calc_reac_deviation(i_reac))
            i_reac += 1
        reac_dev = list(deviations)
        if (len(deviations) > 0):
            self.reac_deviation = Deviation(deviations)
        else:
            self.reac_deviation = False
        if (self.reac_deviation and self.dist_deviation):
            fitness = float(self.w_distances * self.dist_deviation.rmsd + \
                            self.w_reactions * self.reac_deviation.rmsd)
        elif(self.reac_deviation and not self.dist_deviation):
            fitness = float(self.w_reactions * self.reac_deviation.rmsd)
        elif(not self.reac_deviation and self.dist_deviation):
            fitness = float(self.w_distances * self.dist_deviation.rmsd)
        else:
            sys.exit('Error: No fitness observables defined. PSO ' + \
                     'cannot optimize parameters without references.')
        self._fitness.append(fitness)


    def _calc_dist_deviation(self, i_dist):
        '''Calculates one distance value for given index.'''
        mol_name = self.training.distances[i_dist].get_name()
        dir_name = ''
        for molecule in self.training.molecules:
            if (molecule.get_lname() == mol_name):
                dir_name = '%s' % molecule.get_folder()
                break
        atoms = self.training.distances[i_dist].get_atoms()
        dist = self.calculator.get_distance(mol_name, dir_name, atoms).get_val()
        deviation = self.training.distances[i_dist].get_deviation(dist)
        return deviation


    def _calc_reac_deviation(self, i_reac):
        '''Calculates one distance value for given index.'''
        molecules = self.training.reactions[i_reac].get_molecules()
        factors = self.training.reactions[i_reac].get_factors()
        reaction = training.Reaction(molecules, factors)
        mol_list = []
        for mol_name in molecules:
            for mol in self.training.molecules:
                if (mol.get_lname() == mol_name.lower()):
                    mol_list.append(mol)
        reaction.update(mol_list)
        tmp_e = reaction.get_energy()
        deviation = self.training.reactions[i_reac].get_deviation(tmp_e)
        return deviation


class Deviation(object):
    '''Class that calculates common error/deviation values.'''

    def __init__(self, deviations):
        '''Initializes the instance and calculates all error values.'''
        self.msd = self._calculate_msd(deviations)
        self.mad = self._calculate_mad(deviations)
        self.rmsd = self._calculate_rmsd(deviations)
        self.median = self._calculate_median(deviations)


    @staticmethod
    def _calculate_msd(deviations):
        '''Calculates the mean signed deviation.'''
        msd = 0.0
        for entry in deviations:
            msd += entry
        msd = msd / len(deviations)
        return msd


    @staticmethod
    def _calculate_mad(deviations):
        '''Calculates the mean absolute deviation.'''
        mad = 0.0
        for entry in deviations:
            mad += abs(entry)
        mad = mad / len(deviations)
        return mad


    @staticmethod
    def _calculate_rmsd(deviations):
        '''Calculates the root mean square absolute deviation.'''
        from math import sqrt
        rmsd = 0.0
        for entry in deviations:
            rmsd += entry * entry
        rmsd = sqrt(rmsd / len(deviations))
        return rmsd

    @staticmethod
    def _calculate_median(deviations):
        '''Calculates the median of all deviations.'''
        nn = len(deviations)
        if (nn % 2 == 1):
            ## The index should be ((nn - 1) / 2 + 1), but since python
            ## lists start with index 0 we got (... + 1 - 1)
            median = deviations[int((nn - 1) / 2)]
        else:
            ## The index should be nn / 2 but since python
            ## lists start with index 0 we got (... - 1)
            median = deviations[int(nn / 2 - 1)]
        return median
