'''
calculator -- Enables the use of different calculators inside psofit.
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

import os
import subprocess
import sys
from . import training
from ase import io

class Calculator(object):
    '''Handler class initializing the requested calculator.'''

    def __init__(self, calc_name, executable, input_template):
        '''Initializes the calculator object and picks the correct
        calculator.'''
        ## Class constants
        self.SUPPORTED_CALCULATORS = ['DFTB+']
        ## Initialize the calculator object
        if os.path.exists(executable):
            self.executable = executable
        else:
            sys.exit('Error: Executable file not found: %s' % executable)
        self.input_template = input_template
        ## Check calculator support and create instance
        self.calc_name = calc_name
        if (self.calc_name == 'DFTB+'):
            self.calc = DFTBPlus(self.executable, self.input_template)
        else:
            sys.exit('Error: Unsopported calculator: %s\n' % self.calc_name + \
                     'Supported calculators: %s' % \
                     ', '.join(self.SUPPORTED_CALCULATORS))

    def run(self, path, coord, values):
        '''Runs the calculator in the given path for the input
        parameter and particle values.'''
        if (not self.calc.run(path, coord, values)):
            sys.exit('Error: %s calculation failed.' % self.calc_name)
        return None


    def get_energy(self, dir_name):
        '''Returns the energy for given molecule dir_name.'''
        return self.calc.get_energy(dir_name)


    def get_distance(self, mol_name, dir_name, atoms):
        '''Returns the distance for given molecule and two given atoms.'''
        return self.calc.get_distance(mol_name, dir_name, atoms)


class DFTBPlus(object):
    '''Object that can run DFTB+ calculations and return results.'''

    def __init__(self, executable, input_template):
        '''Initializes the DFTB+ instance.'''
        self.CALC_OUT = 'dftb.out'
        self.executable = executable
        self.base_dir = os.getcwd()
        self.template = self._read_template(input_template)


    def run(self, path, coord, values):
        '''Runs a DFTB+ calculation in the given directory.'''
        self._cleanup_previous()
        if (not self._create_input(path, coord, values)):
            sys.exit('Error: Failed to create DFTB+ input file.')
        try:
            os.chdir('%s/%s' % (self.base_dir, path))
            with open(self.CALC_OUT, 'w') as dftb_out:
                try:
                    subprocess.check_call('%s' % self.executable,
                                          stdout=dftb_out,
                                          stderr=dftb_out)
                except subprocess.CalledProcessError:
                    sys.exit('Error: Calculator DFTB+ exited with non-zero ' + \
                             'exit status.')
            os.chdir(self.base_dir)
        except IOError:
            sys.exit('Error: Could not open %s/dftb.out ' % path + \
                     'file to write executable output.')
        return True


    def get_energy(self, dir_name):
        '''Calculates the energy in the given molecule.'''
        path = '%s/%s' % (self.base_dir, dir_name)
        try:
            with open('%s/detailed.out' % path) as dftb_out:
                for line in dftb_out:
                    if (line.find('Total energy:') >= 0):
                        energy = float(line.split()[2])
        except IOError:
            sys.exit('Error: Could not open %s/detailed.out for reading.' % \
                     path)
        return float(energy)


    def get_distance(self, mol_name, dir_name, atoms):
        '''Calculates the distance between the given two atoms in molecule
        and returns a non-reference distance object.'''
        path = '%s' % dir_name
        ## TODO: get the output prefix from the input file, don't presume a
        ## certain name (because it's subject to change)
        geo_file = '%s/geo_end.gen' % path
        ## TODO: Read from input wether it's geometry optimization
        ## or a single-point (which does not produce a output file).
        if (not os.path.exists(geo_file)):
            geo_file = '%s/in.gen' % path
        geometry = io.read(geo_file, format='gen')
        distance = float(geometry.get_distance(atoms[0], atoms[1]))
        return training.Distance(mol_name, atoms, ref=False, val=distance)


    ## Private methods, all may be subject to major changes
    def _write_file(self, content, path):
        '''Writes a list of strings into file given by the path.'''
        try:
            with open(path, 'w') as out_file:
                for line in content:
                    out_file.write('%s\n' % line)
        except IOError:
            sys.exit('Error: Could not write to %s' % path)
        return True


    @staticmethod
    def _read_template(file_path):
        '''Reads a template file, removes comments and returns each
        line as a string, stripped from all empty elements.'''
        with open(file_path, 'r') as ff:
            in_file = ff.readlines()
            in_file = [line[:line.find('#')] for line in in_file]
            in_file = [x for x in in_file if x.strip()]
        return list(in_file)


    def _cleanup_previous(self):
        '''Cleans up output files from previous calculations in the current
        directory.'''
        file_list = ['band.out', 'charges.bin', 'detailed.out', 'dftb_in.hsd',
                     'dftb_pin.hsd', self.CALC_OUT]
        for tmp_file in file_list:
            if os.path.exists('./%s' % tmp_file):
                try:
                    os.remove('./%s' % tmp_file)
                except OSError:
                    sys.exit('Error: Could not delete file %s.' % tmp_file)
        return None


    def _create_input(self, path, coord, values):
        '''Uses the input template and replaces all given names in the
        coordinates within the file the corresponding values.'''
        self.out_file = []
        occurrences = 0
        for line in self.template:
            ii = 0
            ## For each line, search for all possible names and replace them
            while (ii < len(coord)):
                i_param = line.lower().find(coord[ii]['name'].lower())
                if (i_param >= 0):
                    occurrences += 1
                    line = str(line[:i_param]) + str(values[ii])
                ii += 1
            self.out_file.append(line)
        if (occurrences > len(values)):
            sys.exit('Error: Too many matches for PSO fit parameters in ' + \
                     'DFTB+ input file template!')
        elif (occurrences < len(values)):
            sys.exit('Error: Not enough matches for PSO fit parameters in ' + \
                     'DFTB+ input file template!')
        else:
            file_path = '%s/%s/dftb_in.hsd' % (self.base_dir, path)
            self.dftb_in = list(self.out_file)
            if (self._write_file(self.out_file, file_path)):
                return True
            else:
                return False
