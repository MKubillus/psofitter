'''
psofit
A python package that enables fitting of parameters for DFTB via
particle swarm optimization (PSO), using DFTB+.
'''

# This file is part of the PSOFit package, licensed under the GNU General
# Public License (GPL) 3. For more Info see LICENSE in the root directory.
# Maximilian Kubillus, 2015
# maximilian.kubillus@gmail.com

__author__ = 'Maximilian Kubillus'
__version__ = 'v0.1'

__all__ = ['calculator', 'output', 'psofit', 'swarm', 'training']

from . import calculator
from . import output
from . import swarm
from . import training
from .psofit import PSOFit

################################################################################
#
#  TO-DOs:
#
#    - Add documentation about expected input/output for each method/function
#    - Extract all parser routines and create a parser module parser.py
#    - Create exc_psofit module with correct exception handling
#    - Create unit tests
#    - Remove the TrainingSet, and Output classes and rewrite to pure function
#      modules
#    [x] Move to python3 as soon as ASE does (everything should already be
#      written py3 compatible -- ASE 3.9.x has preliminary py3 support)
#
################################################################################
